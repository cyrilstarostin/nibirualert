package com.ltst.nibirualert.presentation.base.exception

import com.ltst.nibirualert.R
import com.ltst.nibirualert.data.network.exception.NoInternetException
import com.ltst.nibirualert.data.network.exception.NotFoundException

internal class ExceptionInfoMapper {

    fun map(throwable: Throwable) = ExceptionInfo(
        throwable,
        mapErrorMessage(throwable)
    )

    private fun mapErrorMessage(exception: Throwable) = when (exception) {
        is NoInternetException -> R.string.error_message_no_internet
        is NotFoundException -> R.string.error_message_not_found
        else -> R.string.error_message_unknown
    }
}
