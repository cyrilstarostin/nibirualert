package com.ltst.nibirualert.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ltst.nibirualert.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
