package com.ltst.nibirualert.data.network.exception

import okhttp3.Response

class ServerExceptionFactory {

    fun createFromResponse(response: Response): Exception {
        return when (response.code()) {
            422 -> UnprocessableEntityException()
            404 -> NotFoundException()
            in 500 until 600 -> InternalServerErrorException()
            // TODO: maybe add more common exceptions, or create custom ones for the specific server
            else -> UnknownServerException()
        }
    }
}
