package com.ltst.nibirualert.data.network.exception

import java.io.IOException

class NoInternetException : IOException(ERROR_MESSAGE) {

    companion object {
        private const val ERROR_MESSAGE = "No internet connection"
    }
}

sealed class ServerException(message: String) : IOException(message)

class UnknownServerException : ServerException(LOG_MESSAGE) {
    companion object {
        private const val LOG_MESSAGE = "An unknown server exception occurred"
    }
}

class InternalServerErrorException : ServerException(LOG_MESSAGE) {
    companion object {
        private const val LOG_MESSAGE = "An internal server error occurred"
    }
}

class UnprocessableEntityException : ServerException(LOG_MESSAGE) {
    companion object {
        private const val LOG_MESSAGE = "422 exception occurred, could not identify it"
    }
}

class NotFoundException : ServerException(LOG_MESSAGE) {
    companion object {
        private const val LOG_MESSAGE = "404 exception occurred, could not identify it"
    }
}
